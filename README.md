# Programmieraufgabe in C für GINF (SS 2019), Gruppe 2

## Aufgabenstellung

Es soll ein Programm in der Programmiersprache C entworfen werden, das beigegebener Abstandsmatrix das Single-Source-Shortest-Path Problem löst.

### Aufgabe 1

Schreiben Sie zunächst ein (Teil-)Programm, mit dem es möglich ist eine Ab-standsmatrix beliebiger Größe eingeben zu können.

| Hinweise zu Aufgabe 1|
| ------ |
| Die bezeichnung der einzelnen Knoten muss frei wählbar sein |
| Die EIngabeschnittstelle muss Selbsterklärend sein |

### Aufgabe 2

Implementieren Sie nun den Bellmann-Ford-Algorithmus zur Lösung des Single Source Shortest Path Problems

| Hinweise zu Aufgabe 2 |
| ------ |
| Basis zur Berechnung der Abstände bildet die Abstandsmatrix aus Aufgabe 1 |
| Der Startknoten muss frei wähblar sein |

### Hinweise

| Hinweise zur Programmausgabe |
| ------ |
| Aus der Programmausgabe sollten folgende Dinge ersichtlich sein: |
| Aufbau der Abstandsmatrix |
| Ausgabe des Startknotens |
| Vor dem Start des Algorithmus soll die Tabelle (wie in der Vorlesung mit Iteration, Vorgänger und Kosten) angezeigt werden |
| Zeigen Sie zwischenergebnisse, d.h. die Tabelle muss nach jeder Iteration neu angezeigt werden. Hilfreich wäre zudem ein Hinweis, bei welchem Knoten es Veränderungen gab |
| Anzeige der Endtabelle. Hier muss Das Endergebnis der Tabelle und die Anzahl der Iterationen ersichtlich sein. |

| Anforderungen an die Ausarbeitung |
| ------ |
| Bei der Ausarbeitung Ihrer Dokumentation müssen folgende Punkte ersichtilch sein |
| Programmcode (incl. Kommentare) |
| Programmausgabe |
| Kurze Beschreibung ihres Quellcodes |
| Angabe von Quellen |

| Hinweise zur Ausarbeitung |
| ------ |
| Ihre Ausarbeitung muss keine "perfekte" Dokumentation sein, Dennoch sollten Sie auf korrektes Deutsch achten. |
| In Ihrer Ausarbeitung sollen die Funktionen, die sie verwenden, kurz beschrieben werden. Es muss nicht jede Zeile erklärt werden, aber für jeder Funktion soll erklärt werden, wie diese Funktiuoniert und wofür sie verwendet wird. |
| Der Code, den SIe entwickelt haben, muss nachvollziehbar sein |

| Hinweise zur Abgabe |
| ------ |
| Pro Gruppe ist eine Ausarbeitung bis zum Abgabedatum beim Kursleiter abzugeben |
| Das ablauffähige Programm (.exe **und** .sh) und die Ausarbeitung sind per **E-Mail** abzugeben. |
| Der Vollständige C-Code ist als Datei abzugeben. |