import java.io.InputStream;
import java.util.Scanner;

public class BellmannFord {

	//Final Variablen
	static final int maxKnoten = 100;
	static final int maxZeichen = 10;
	static final int infinity = 99;

	//Variablen
	static int anzKnoten, startKnoten, anzIterationen;
	static int[] vorgängerMatrix, kostenMatrix;
	static int[][] abstandsMatrix;
	static Scanner scan;
	static char[][] bezeichnungKnoten;


	public static void main(String[] args) {
		init();

		System.out.println("------------Bellmann-Ford------------");


		erstelleAbstandsmatrix();

		wähleStartknoten();

		bellmann_Ford();


	}

	static void init() {
		bezeichnungKnoten = new char[maxKnoten][maxZeichen];
		abstandsMatrix = new int[maxKnoten][maxKnoten];
		kostenMatrix = new int[maxKnoten];
		vorgängerMatrix = new int[maxKnoten];
		scan = new Scanner(System.in);
	}

	static void erstelleAbstandsmatrix() {
		System.out.println("Wie viele Knoten soll der Graph haben?");
		anzKnoten = scan.nextInt();

		System.out.println("Bitte geben Sie jedem Knoten eine Bezeichnung.");
		for (int i = 0; i < anzKnoten; i++) {
			System.out.print("Knoten " + (i + 1) + ": ");
			bezeichnungKnoten[i] = scan.next().toCharArray();
		}

		for (int i = 0; i < anzKnoten; i++) {
			for (int j = 0; j < anzKnoten; j++) {
				if (i == j) {
					abstandsMatrix[i][j] = 1;
				} else {
					abstandsMatrix[i][j] = infinity;
				}
			}
		}

		for (int i = 0; i < anzKnoten; i++) {
			System.out.println((i + 1) + ". " + String.valueOf(bezeichnungKnoten[i]));
		}

		System.out.println("Bitte geben Sie nun die Kanten ein:");
		System.out.println("[Index Kante 1]/[Index Kante 2]/[Gewicht]    (Geben sie [w] ein wenn sie keine weiteren Kanten mehr hinzufügen wollen");
		String[] input = new String[3];
		do {
			input = scan.next().split("/");
			if (!input[0].equals("w")) {
				abstandsMatrix[(Integer.parseInt(input[0]) - 1)][(Integer.parseInt(input[1]) - 1)] = Integer.parseInt(input[2]);
			}
		} while (!input[0].equals("w"));

		ausgabeAbstandsmatrix();

	}

	static void ausgabeAbstandsmatrix() {
		System.out.print("   ");
		for (int i = 0; i < anzKnoten; i++) {
			System.out.print("   [" + String.valueOf(bezeichnungKnoten[i]) + "]");
		}
		System.out.println();
		for (int i = 0; i < anzKnoten; i++) {
			System.out.print("[" + String.valueOf(bezeichnungKnoten[i]) + "]    ");
			for (int j = 0; j < anzKnoten; j++) {
				System.out.print((abstandsMatrix[i][j] == infinity) ? "n    " : abstandsMatrix[i][j] + "     ");
			}
			System.out.println();
		}
	}

	static void wähleStartknoten() {
		System.out.println();
		System.out.println("Wählen Sie einen Startknoten");
		for (int i = 0; i < anzKnoten; i++) {
			System.out.println((i + 1) + ". " + String.valueOf(bezeichnungKnoten[i]));
		}
		startKnoten = scan.nextInt() - 1;



		System.out.println("Startknoten: " + String.valueOf(bezeichnungKnoten[startKnoten]));
	}


	static void bellmann_Ford() {
		System.out.println("Iteration  Knoten:   Vorgänger:");
		System.out.print("           ");
		System.out.print(String.valueOf(bezeichnungKnoten[startKnoten]) + "  ");
		for (int i = 0; i < anzKnoten; i++) {
			if (i != startKnoten)
				System.out.print(String.valueOf(bezeichnungKnoten[i]) + "  ");
		}
		System.out.print("   ");
		for (int i = 0; i < anzKnoten; i++) {
			System.out.print(String.valueOf(bezeichnungKnoten[i]) + "  ");
		}


		for (int i = 0; i < anzKnoten; i++) {
			kostenMatrix[i] = infinity;
			if (i == startKnoten) kostenMatrix[i] = 0;
			vorgängerMatrix[i] = -1; //-1 bedeutet es gibt noch keinen Vorgänger
		}
		anzIterationen = 0;

		System.out.println();
		System.out.println("    " + anzIterationen);
		ausgabeKnotenUndVorgänger();

		boolean kostenGeändert = true;	//### Was ist b? Findung von Endlosschleifen?
		while (kostenGeändert) { //HIER FÄNGT DER BELLMANNFORD EIGENTLICH AN ... xD
			kostenGeändert = false;
			System.out.println();
			System.out.println("    " + anzIterationen);

			//für alle Knoten außer Startknoten
			for (int j = 0; j < anzKnoten; j++) { //Habe den Startknoten nach vorne Gezogen da Inan das in seiner Folie auch so gemacht hat
				if (startKnoten != j) {	
					if (kostenMatrix[j] > abstandsMatrix[startKnoten][j]) {	//Wenn die Kosten von startKnoten → Knoten j < unendlich, überschreibe in Kostenmatrix.
						kostenMatrix[j] = abstandsMatrix[startKnoten][j];
						vorgängerMatrix[j] = startKnoten; //Die Verbindung vom Knoten j zum startKnoten wird notiert.
						kostenGeändert = true; //### b wieder... was sagt mir das :'D
					}
				}

			}
			ausgabeKnotenUndVorgänger();
			//Hier das selebe nochmal nur ohne Startknoten und cooler :P ### "und cooler" ... uhhhhm :'D??

			
			for (int i = 0; i < anzKnoten - 1; i++) { //### warum -1?
				for (int j = 0; j < anzKnoten; j++) {
					if (i == startKnoten) i++;
					if (i == j && i != anzKnoten - 2) j++;

					if (kostenMatrix[j] > abstandsMatrix[i][j] + kostenMatrix[i]) {
						kostenMatrix[j] = abstandsMatrix[i][j] + kostenMatrix[i];
						vorgängerMatrix[j] = i;
						kostenGeändert = true;
					}

				}
				ausgabeKnotenUndVorgänger();
			}


			anzIterationen++;
			
		}

	}





	static void ausgabeKnotenUndVorgänger() { //Krasser Methodenname :D //TODO bitte etwas einfallsreicheres Einfügen
		System.out.print("           ");
		System.out.print(kostenMatrix[startKnoten] + "  ");
		for (int i = 0; i < anzKnoten; i++) {
			if (i != startKnoten)
				System.out.print(kostenMatrix[i] + "  ");
		}
		System.out.print("   ");
		for (int i = 0; i < anzKnoten; i++) {
			System.out.print((vorgängerMatrix[i] == -1) ? "-  " : String.valueOf(bezeichnungKnoten[vorgängerMatrix[i]]) + "  ");
		}
		System.out.println();
	}



}