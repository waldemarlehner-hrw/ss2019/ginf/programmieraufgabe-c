/*
 *  Diese Datei wird nur zum Testen der userInput Headerdatei verwendet. Sie ist nicht fuer die Aufgabe relevant.
 * 
 */
#include "../src/userInput.h"

//#define TEST_CHAR
//#define TEST_BOOLEAN
//#define TEST_INT
#define TEST_FLOAT


int main(void){
	while(true){

	
	#ifdef TEST_STRING
	char buffer[21];
	userInput_getString(buffer,22);
	printf("Der zurueckgegebene String ist: \n%s\n",buffer);
	char i;
	scanf("%c",i);
	#endif

	#ifdef TEST_CHAR
	char c = userInput_getChar();
	printf("Charwert: %c\n",c);

	scanf("%c");
	#endif

	#ifdef TEST_BOOLEAN
	int boolean = userInput_getBoolean();
	printf("Boolean: %i\n",boolean);
	scanf("%c");
	#endif

	#ifdef TEST_INT
	int retval = userInput_getInt(false);
	printf("Int: %i\n",retval);
	#endif

	#ifdef TEST_FLOAT
	float retval_ = userInput_getFloat(false);
	printf("Float: %f\n",retval_);
	#endif
	}
}