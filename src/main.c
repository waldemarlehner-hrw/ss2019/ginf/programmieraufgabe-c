#include "userInput.h"
#include "output.h"
#include "color.h"

//überspringe matrix aufsetzung um beim testen nicht immer den Spaß eingeben zu müssen
//#define SKIPMATRIXINIT



int main(void){
	//Wenn Windows, hol dir die Konsolenparameter, um farben manipulieren zu können
	#ifdef IS_WINDOWS_ENVIRONMENT
		windowsColorHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		GetConsoleScreenBufferInfo(windowsColorHandle,&consoleInfo);
		defaultConsoleAttribures = consoleInfo.wAttributes;
	#endif


	consoleChangeColor(Info);
	printInitOutput();
	consoleChangeColor(Default);
	#pragma region Aufsetzen der Matrix
	#ifndef SKIPMATRIXINIT
	printf("Wie viele Knoten soll die Distanzmatrix repr%csentieren?\n",ae);
	int vertexCount = userInput_getInt(true,false);
	
	
	clear();
	printf("Sollen gerichtete Kanten verwendet werden?\n");
	int useDirectedEdges = userInput_getBoolean(); //Gibt an ob in die Matrix "ungerichtete" (also arr[a,b] = arr[b,a] = x) oder gerichtete (arr[a,b] = x) Gewichte geschrieben werden sollen 

	while(vertexCount < 2){
		consoleChangeColor(Error);
		printf("[FEHLER] Man ben%ctigt mindestens 2 Knoten. Bitten geben wie einen Wert gr%c%cer als 1 an.\n",oe,oe,ss);
		consoleChangeColor(Default);
		vertexCount = userInput_getInt(true,false);
	}
	int distanceMatrix[vertexCount*vertexCount];
	int i = 0;

	for(i;i<vertexCount*vertexCount;i++){
		distanceMatrix[i] = (i%(vertexCount+1) == 0)?0:INT_MAX; //Unendlich als INT_MAX definiert. Fülle mit Unendlich auf. 
	}
	//Überschreibe nun noch die Hauptdiagonale mit Nullen

	clear();
	//Erstelle einen CharakterBuffer. Dabei hat jeder Knoten eine Namen mit einer maximallänge von 20 chars + \0.
	char vertexNames[vertexCount*21];
	//Frage den Nutzer ob er Namen für die Knoten eingeben will.
	printf("M%cchten Sie den Knoten Namen vergeben?\n",oe);
	if(userInput_getBoolean()){
		//Der Nutzer möchte die Namen selbst vergeben.
		int i=0; //Iterator für die Aktuelle Eingabe
		int j=0; //Iterator um frühere eingaben Darzustellen
		for(i;i<vertexCount;i++){
			clear();
			for(j;j<i;j++){
				printf("[%i] >\t ",j);
				int offset = 0;
				while(vertexNames[(21*j)+offset] != NUL){
					printf("%c",vertexNames[(21*j)+offset]);
					offset++;
				}
				printf("\n");

			}
			j = 0;
			printf("\n\n\n");
			printf("Geben Sie den Namen f%cr den Knoten Nr %i ein. Eine leere Eingabe erstellt einen automatischen Namen.\n",ue,i);
			char name[21];
			userInput_getString(name,20,true);
			if(name[0] == NUL){
				//Die Eingabe war leer. Erstelle eigenen Namen
				sprintf(name,"Knoten %i",i);
				
			}
			//Jetzt ist entweder der vom Nutzer eingegebene oder autogenerierte Name im name-Char Array
			//Füge nun den Namen zum vertexNames Array hinzu.
			sprintf(vertexNames+21*i,"%s",name);
		

		}
	}else{
		//Vergebe die Namen 'Knoten X' (X = index)
		int i = 0;
		for(i;i<vertexCount;i++){
			char name[21];
			sprintf(name,"Knoten %i",i);
			
			sprintf(vertexNames+21*i,name);
		}
	}
	//Zeige die Matrix
	DistanceMatrixConf:
	clear();
	printDistanceMatrixLegend();
	printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);
	//Die Matrix wurde angezeigt. Jetzt darf sie vom Nutzer manipuliert werden.
	//Drücke einen Hübschen Header aus.
	consoleChangeColor(Info);
	printf(
		"\n\n\n"
		"================================\n"
		"Konfiguration der Abstandsmatrix\n"
		"================================"
		"\n\n\n");
	consoleChangeColor(Default);
	int userWantsToContinueConfiguration = true;
	while(userWantsToContinueConfiguration){
		
		if(useDirectedEdges)
		printf(" ? -> ? hat ein Kantengewicht von ?\n");
		else
		printf(" ? <-> ? hat ein Kantengewicht von ?\n");
		consoleChangeColor(Gray);
		printf("Geben sie den ersten Knoten der Verbindung an. Erlaubte Eingabe: [%i ... %i]\n",0,vertexCount-1);
		consoleChangeColor(Default);
		int knoten1ID = userInput_getInt(true,true);
		while(knoten1ID  > vertexCount-1){
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht gr%c%cer als %i sein.\n",oe,ss,vertexCount-1);
			knoten1ID = userInput_getInt(true,true);
		}
		consoleChangeColor(Default);
		if(useDirectedEdges)
		printf(" %i -> ? hat ein Kantengewicht von ?\n",knoten1ID);
		else
		printf(" %i <-> ? hat ein Kantengewicht von ?\n",knoten1ID);
		consoleChangeColor(Default);
		int knoten2ID = userInput_getInt(true,true) ;
		while(knoten2ID > vertexCount || knoten2ID == knoten1ID){
			consoleChangeColor(Error);
			if(knoten1ID == knoten2ID){
				printf("[FEHLER] Die beiden Knoten d%crfen nicht identisch sein. Bitte geben Sie f%cr den zweiten Knoten einen anderen Knoten als [%i] an\n",ue,ue,knoten1ID);
			}
			else{
			printf("[FEHLER] Die Eingabe darf nicht gr%c%cer als %i sein.\n",oe,ss,vertexCount-1);
			}
			knoten2ID = userInput_getInt(true,true) ;
		}
		consoleChangeColor(Default);
		if(useDirectedEdges)
		printf(" %i -> %i hat ein Kantengewicht von ?\n",knoten1ID,knoten2ID);
		else
		printf(" %i <-> %i hat ein Kantengewicht von ?\n",knoten1ID,knoten2ID);
		consoleChangeColor(Default);
		printf("Geben sie ein Kantengewicht als ganze Zahl an. Mit 0 trennen Sie die Verbindung zweier Knoten!\n");
		int kantenGewicht = userInput_getInt(false,true);
		if(kantenGewicht == 0){
			kantenGewicht = INT_MAX;
		}
		consoleChangeColor(Info);
		printf("\n\n\n\n##################\n");
		printf("Stimmt diese Eingabe?\n");
		
		if(useDirectedEdges)
		printf(" %i -> %i hat ein Kantengewicht von %i\n",knoten1ID,knoten2ID,kantenGewicht);
		else
		printf(" %i <-> %i hat ein Kantengewicht von %i\n",knoten1ID,knoten2ID,kantenGewicht);
		printf("###\n");
		consoleChangeColor(Default);
		printf("y: Ja, hinzuf%cegen.\n",ue);
		printf("Y: Ja, hinzuf%cgen und nach keinen weiteren Kanten fragen.\n\n",ue);
		printf("n: Nein, nicht hinzuf%cgen.\n",ue);
		printf("N: Nein, und nicht nach weiteren Kanten fragen.\n");
		printf("###\n");
			
		char userInputOperation = userInput_getChar();
		while(
			userInputOperation != 'Y' &&
			userInputOperation != 'y' &&
			userInputOperation != 'N' &&
			userInputOperation != 'n' 
			){
				consoleChangeColor(Error);
				printf("[FEHLER] Gegebene Eingabe muss Y,y,N oder n sein.\n");
				userInputOperation = userInput_getChar();
				consoleChangeColor(Default);
		}
		if(userInputOperation == 'y' || userInputOperation == 'Y'){
			distanceMatrix[knoten1ID*vertexCount+knoten2ID] = kantenGewicht;
			if(!useDirectedEdges){
			distanceMatrix[knoten2ID*vertexCount+knoten1ID] = kantenGewicht;
			}
			if(userInputOperation == 'Y'){
				userWantsToContinueConfiguration = false;
			}
		}
		if(userInputOperation == 'N'){
			userWantsToContinueConfiguration = false;
		}
		clear();
		printDistanceMatrixLegend();
		printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);



	}
	#pragma endregion
	#else
	const int vertexCount = 5;
	int distanceMatrix[vertexCount*vertexCount];
	for(int i=0;i<vertexCount*vertexCount;i++){
		distanceMatrix[i] = (i%(vertexCount+1)==0)?0:INT_MAX;
	}
	distanceMatrix[1] = 1;
	distanceMatrix[2] = 5;
	distanceMatrix[3] = 5;
	distanceMatrix[8] = 1;
	distanceMatrix[11] = 2;
	distanceMatrix[14] = 2;
	distanceMatrix[19] = 2;
	
	char vertexNames[21*vertexCount];
	for(int i=0;i<vertexCount;i++){
		char name[21];
		sprintf(name,"Knoten %i",i);
		
		sprintf(vertexNames+21*i,name);
	}
	int useDirectedEdges = 1;
	printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);
	#endif
	#pragma region Bellman-Ford-Implementierung
	//Frage den Nutzer für den Startknoten
	printf("Bitte geben Sie den Startknoten an.\n");
	int startKnoten = userInput_getInt(true,true);
	while(startKnoten > vertexCount-1){
		printf("[FEHLER] Die Eingabe darf nicht gr%c%cer als %i sein.\n",oe,ss,vertexCount-1);
		startKnoten = userInput_getInt(true,true);
	}
	// Zuerst werden die Kosten- und Vorgängermatrizen initialisiert
	// Code übernommen auf Maiks Java-Implementierung
	int kostenMatrix[vertexCount];
	int kostenMatrixLetzteIteration[vertexCount];
	int vorgaengerMatrix[vertexCount];
	int vorgaengerMatrixLetzteIteration[vertexCount];
	//Fülle alle Werte mit UNENDLICH in der kostenMatrix bzw -1 (bedeutet noch keine Vorgänger)
	//aka. die "initialisierung"
	for(int i = 0;i<vertexCount;i++){
		kostenMatrix[i] = kostenMatrixLetzteIteration[i] = (i==startKnoten)?0:INT_MAX;
		vorgaengerMatrix[i] = vorgaengerMatrixLetzteIteration[i] = -1;
	}
	//TODO: Ausgabe der Iteration
	int iterationZaehler = 0;
	int habenSichDieKostenGeaendert = true; // Boolean
	clear();
	printIterationLegend();
	printIterationHeader(startKnoten,vertexCount);
	printIterationRow(iterationZaehler,vertexCount,startKnoten,kostenMatrix,kostenMatrixLetzteIteration,vorgaengerMatrix,vorgaengerMatrixLetzteIteration);
	//Implementierung des Pseudocode auf https://brilliant.org/wiki/bellman-ford-algorithm/
	iterationZaehler = 1;
	//von 1 bis |V| - 1
	for(iterationZaehler;iterationZaehler<=(vertexCount-1);iterationZaehler++){
		int gabEsAenderungen = false;
		//Gehe nun durch jede Kante
		// kantenAnfange 	:= der Index des Knotens, wo die Verbindung z (A >-(z)--> B) beginnt
		// kantenEnde 		:= der Index des Knotens, wo die Verbindung z (A >-(z)--> B) endet
		for(int kantenAnfang = 0;kantenAnfang < vertexCount; kantenAnfang++){										
			for(int kantenEnde = 0; kantenEnde < vertexCount; kantenEnde++){										
				//Es kann keine Verbindunge mit sich selbst geben. Überspringe. (a → a unmöglich)
				if(kantenAnfang == kantenEnde){
					continue;
				}
				int kantenGewicht = distanceMatrix[getLinearizedArrayIndex(kantenAnfang,kantenEnde,vertexCount)];  	
				int kantenAnfangAbstand = kostenMatrix[kantenAnfang];												
				int kantenEndeAbstand = kostenMatrix[kantenEnde];													
				// Man könnte versuchen, kantenAnfangsAbstand + kantenGewicht zu nehmen. Jedoch gäbe es hier eventuell einen int overflow wenn einer von den
				//beiden Werten als unendlich definiert ist, woduch die Bedingung
				// sehr warscheinlich stimmen würde (wegen overflow, wird zu seeehr kleinen negativen Zahl). deswegen wird zuerst auf unendlichkeit überprüft.
													//Es gibt keine Verbindung A --> B, da das Gewicht auf unendlich gesetzt ist.
				if(kantenAnfangAbstand != INT_MAX && kantenGewicht != INT_MAX){
					// A:= kantenAnfang; B:= kantenEnde
					//Wenn es effizienter ist, über A >-(kante)--> B anstatt ? >-(?)--> B, wird der weg über A bevorzugt.
					//In B wird A als Vorgänger definiert
					if(kantenAnfangAbstand + kantenGewicht < kantenEndeAbstand){
						kostenMatrix[kantenEnde] = kantenAnfangAbstand + kantenGewicht;
						vorgaengerMatrix[kantenEnde] = kantenAnfang;
						gabEsAenderungen = true;

					}
				}
		

			}
			printIterationRow(iterationZaehler, vertexCount,startKnoten,kostenMatrix,kostenMatrixLetzteIteration,vorgaengerMatrix,vorgaengerMatrixLetzteIteration);
			//Überschreibe die alte Kosten- und Vorgängermatrix mit der neuen, somit in dieser Iteration die Änderungen zu sehen ist
			overwriteArray(kostenMatrixLetzteIteration,kostenMatrix,vertexCount);
			overwriteArray(vorgaengerMatrixLetzteIteration, vorgaengerMatrix, vertexCount);
		}
		printf("\n");
		if(!gabEsAenderungen){
			//Man muss hier nicht auf negative Zyklen überprüfen, da es bei negativen Zyklen in jeder Iteration eine Änderung gibt.
			goto NachBellmanFord;
		}
	}
	//Überprüfe nun auf negative Zyklen in dem noch mal ein |V|-tes mal durchgegangen wird und auf Änderungen überprüft wird.
	
	for(int kantenAnfang = 0;kantenAnfang < vertexCount; kantenAnfang++){
		for(int kantenEnde = 0; kantenEnde < vertexCount; kantenEnde++){

			int kantenGewicht = distanceMatrix[getLinearizedArrayIndex(kantenAnfang,kantenEnde,vertexCount)];
			//Es gibt keine Verbindung A --> B, da das Gewicht auf unendlich gesetzt gibt. Deshalb gibt es auch nichts zu überprüfen.
	
			int kantenAnfangAbstand = kostenMatrix[kantenAnfang];
			int kantenEndeAbstand = kostenMatrix[kantenEnde];
			
			if(kantenGewicht == INT_MAX || kantenAnfangAbstand == INT_MAX || kantenEndeAbstand == INT_MAX){
				continue;
			}

			if(kantenAnfangAbstand + kantenGewicht < kantenEndeAbstand){
				//printf(" A :%i\n|A|:%i\n B :%i\n|B|:%i\n z :%i",kantenAnfang,kantenAnfangAbstand,kantenEnde,kantenEndeAbstand,kantenGewicht);
				goto NegativerZyklus;
			}
		}
	}
	
	//Fehlerhandling. false, damit man nur mit goto reinkommt.
	while(false){
		NegativerZyklus:
		consoleChangeColor(Warn);
			printf("\n[WARNUNG] Negativer Zyklus wurde erkannt. Berechnung fehlgeschalgen.\n");
			consoleChangeColor(Default);
	}
	
	NachBellmanFord:
	printf(""); //label muss auf ein statement zeigen
	int unreachable = 0;
	for(int i = 0; i < vertexCount; i++){
		if (kostenMatrix[i] == INT_MAX){
			unreachable = 1;
		} 
	}
	if(unreachable){
		consoleChangeColor(Warn);
		printf("[WARNUNG] Der Graph ist nicht zusammenh%cngend! Es konnten nicht alle Knoten erreicht werden.\n",ae);
		consoleChangeColor(Default);
	}
	#pragma endregion
	printf("\n\n");
	#ifndef SKIPMATRIXINIT
	//Frage den Nutzer ob er änderungen an der Abstandsmatrix machen will und es nochmal durchführen möchte.
	printf("==================================================================\n"); 
	printf("Die Ausf%chrung ist fertig. M%cchten sie zur%cck zur Abstandsmatrix\nkonfiguration und %cnderungen vornehmen? (Nein beendet das Programm)\n",ue,oe,ue,AE); \
	printf("==================================================================\n"); 
	printf("\n\n");
	if(userInput_getBoolean()){
		//Gehe zur Abstandsmatrixkonfiguration
		goto DistanceMatrixConf;
	}else{
		return EXIT_SUCCESS;
	}
	#else
	scanf("%c");
	#endif
	

}