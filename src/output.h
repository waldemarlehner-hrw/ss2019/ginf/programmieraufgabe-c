// Diese Headerdatei hat die nötigen Funktionen für die Ausgabe; bspw. die Ausgabe der Matrix.
#include <stdio.h>
#include <limits.h> //Benötigt für INT_MAX. Kann unter verschiedenen Systemen verschiedene Werte
#include <math.h>
#include "umlaute.h"
#include "matrix.h"
//Gibt die Gruppennummer und kurze Beschreibung wieder.
void printInitOutput(){
	printf(
		"//////////////////////////////////////////\n"
		"Bellmann-Ford-Algorithmus\n"
		"Gruppe 2 - Sommersemester 2019\n"
		"//////////////////////////////////////////\n"
		"\n\n\n"
	);
}

//Gibt die Länge einer Zahl in Charakteren zurück. Benötigt für die Darstellung in der Tabelle
int getIntLen(int val){
	if(val == 0 || val == INT_MAX){ //0 und Unendlich werden jew. durch 1 Char repräsentiert.
		return 1;
	}
	int isPositive = true;
	if(val < 0){
		isPositive = false;
		val *= -1;
	}
	int len = (int)log10(val)+1;
	if(!isPositive){
		len++;
	}
	return len;
}
int getMaxNameLen(char nameBuffer[],int vertexCount){
	int i = 0;
	int maxlen = 0;
	for(i;i<vertexCount;i++){

		int len = 0;
		while(nameBuffer[21*i+len] != NUL){
			len++;
		};
		if(len > maxlen){
			maxlen = len;
		}
	}
	return maxlen;
}


//Gaukelt eine Säuberung der Konsole vor. Es gibt zwar system("clear") bzw system("cls"), jedoch hängt dies von der Umgebung ab.
void clear(){
	int i = 0;
	for(i; i < 5;i++){
		printf("\n\n\n\n\n\n\n\n\n\n");
	}
}


void putXSpaces(int spaceCount){
	while(spaceCount-->0){
		putchar(' ');
	}
}

void printDistanceMatrixLegend(){
		//Legende Darstellen
	consoleChangeColor(Info);
	printf("[Legende]\n");
	consoleChangeColor(Warn);
	printf("*");
	consoleChangeColor(Info);
	printf("  zeigt an, dass es sich um das Element selbst handelt.\n");// = 0
	consoleChangeColor(Gray);
	printf("-");
	consoleChangeColor(Info);
	printf("  zeigt an, dass keine Verbindung zwischen beiden Knoten besteht.\n");	// = INT_MAXVALUE			
	printf("Die gerichtete Kanten zeigen vom Knoten links in der Tabelle zum Knoten oben in der Tabelle\n\n")	;
		

	consoleChangeColor(Default);
}

void printDistanceMatrix(int verticesCount,char namesBuffer[], int distanceMatrixLinearized[]){
	//Speichert für jede Spalte die Maximallänge der Werte (bestimmt mit log_10), um eine Verzerrung der Darstellung der Matrix zu verhindern.
	int maxLenPerColumn[verticesCount];
	int LenOfIndexColumn = (int)(log10(verticesCount)+1); //siehe: https://www.desmos.com/calculator/htt77soqjx
	int LenOfNames = getMaxNameLen(namesBuffer,verticesCount);
	int i = 0;

	//Bekomme die maximalen Längen der einzelnen Spalten
	int spalte = 0; // j = Spalte
	for(spalte;spalte<verticesCount;spalte++){
		//Bekomme den größten Wert aus jeder Spalte
		int maxLen = 0; //Gibt die Maximallänge in dieser Spalte aus
		int isPositive = true;
		int zeile = 0;
		for(zeile;zeile < verticesCount;zeile++){
			int len =  getIntLen( distanceMatrixLinearized[getLinearizedArrayIndex(zeile,spalte,verticesCount)]);
			maxLen = (len > maxLen) ? len : maxLen; 
		}
		maxLenPerColumn[spalte] = maxLen;

	}
	consoleChangeColor(Info);
	//Zeilenkopf aufbauen
	printf("----------------------------------------------------------------------\n");
	putXSpaces(LenOfNames);
	printf("   ");
	putXSpaces(LenOfIndexColumn);
	printf(" | ");
	
	for(int i=0;i<verticesCount;i++){
		printf("[");
		//i = Index des Knotens
		int indexLen = (i > 0)?(int)(log10(i))+1:1;		//Das ist die (Zeichen)länge des Indexes
		int totalLen = maxLenPerColumn[i];						//Das ist die Länge, welche der Eintrag haben muss, um eine schöne Formatierung zu erzeugen
		putXSpaces(totalLen-indexLen);							//Fülle vor der Ausgabe des Indexes mit Leerzeichen voll
		printf("%i]  ",i);
	}
	printf("\n----------------------------------------------------------------------\n");
	consoleChangeColor(Default);
	//Kopf fertig.
	//Drücke nun alle Zeilen aus.
	
	for(int i=0;i<verticesCount;i++){
		#pragma region KnotenName
		int nameLen = 0; //Länge des Namens. Um den Text linksbündig darzustellen und Spaces "vorzufüllen"

		char nameString[21];
		sprintf(nameString,"%s",namesBuffer+21*i);
		//Bekomme die Länge des Namens
		while(nameString[nameLen] != NUL){
			nameLen++;
		}
		//Schreibe den Namen und // zeichen
		consoleChangeColor(Info);
		putXSpaces(LenOfNames-nameLen);
		printf("%s  [",nameString);
		//Bekomme die Maximallänge der Indizes und die Länge des Indexes. Fülle die Differenz mit Leerzeichen auf.
		int indexlen = (i!=0)?(int)log10(i)+1:1;
		putXSpaces(LenOfIndexColumn-indexlen);
		printf("%i]| ",i);
		consoleChangeColor(Default);
		#pragma endregion
		//Der linke Teil ist fertig. Jetzt kommt die Darstellung der einzelnen Werte
		 //Iteriere nach Rechts in der Matrix
		for(int j = 0;j<verticesCount;j++){
			int value = distanceMatrixLinearized[getLinearizedArrayIndex(i,j,verticesCount)];
			int maxLen = maxLenPerColumn[j];
			int valLen; 
			if(value == INT_MAX || value == 0){
				valLen = 1;
			}
			else if(value < 0){
				valLen = (int)log10(-value)+2; //1 mehr wg. Pluszeichen
			}else if(value > 0){
				valLen = (int)log10(value)+1;
			}
			

			if(value == INT_MAX){
				consoleChangeColor(Gray);
				printf("[");
			}
			else if(value == 0){
				consoleChangeColor(Warn);
				printf("[");
			}else{
				printf("[");
			}

			



			putXSpaces(maxLen-valLen);
			if(value == INT_MAX){
				printf("-]  ");
			}
			else if(value == 0){
				printf("*]  ");
			}else{
				printf("%i]  ",value);
			}
			consoleChangeColor(Default);
		}
		
		printf("\n");


	}
	consoleChangeColor(Info);
	printf("----------------------------------------------------------------------\n");
	consoleChangeColor(Default);
	

}

int getMax(int a, int b){
	return (a > b) ? a : b;
}

void formatCols(int j){
	int infty = 0;	//ist Wert unendlich?
	int neg = 0;	//ist Wert negativ?
	if(j == INT_MAX){//Sonderbehandlung für Unendlich (Knoten nicht Erreichbar)
		j = 1;
		infty = 1;
	}
	if(j == -1){ //Sonderbehandlung für -1 (Knoten hat keinen Vorgänger)
		j = 1;
		neg = 1;
	}
    for (int i = 0; i < 2 - getMax(getIntLen(j)/2, 1); i++) //Platzierung in der Mitte der 4 Zeichen
    {
        printf(" ");
    }
	if(infty){
		consoleChangeColor(Gray);
		printf("-");
		consoleChangeColor(Default);
	} else if (neg){
		consoleChangeColor(Gray);
		printf("#");
		consoleChangeColor(Default);
	} else {
		printf("%i",j);
	}
    for (int i = 0; i < 2 - getIntLen(j)/2; i++){
        printf(" ");
    }
}

void formatUpdate(int index, int arr[], int arr_old[]){ //Markiert Updates in der Tabelle
	if(arr[index] != arr_old[index]){
		consoleChangeColor(Warn);
		printf("[");
	} else {
		printf(" ");
	}
	formatCols(arr[index]);
	if(arr[index] != arr_old[index]){
		printf("]");
		consoleChangeColor(Default);
	} else {
		printf(" ");
	}
}

void printIterationHeader(int startVertex, int maxVertices){//Gibt Kopfzeile(n) aus
	consoleChangeColor(Info);
	int table_chars = 0;
    printf("Wird ausgegeben im Format \"Iteration | Kosten | Vorg%cnger\"\n\n",ae);
    printf("Iteration |  ");
    table_chars += 13;
    formatCols(startVertex);
    table_chars+=4;
    printf("  ");
    table_chars+=2;
    for (int j = 0; j < maxVertices; j++){//Fügt die Kosten-Spalten hinzu
        if(j == startVertex){
            continue;
        }
        formatCols(j);
        table_chars+=4;
        if(!(j == maxVertices-1)){
            printf("  ");
            table_chars+=2;
        }
    }
    printf("  | ");
    table_chars += 4;
    for(int j = 0; j < maxVertices; j++){//Fügt die Vorgänger-Spalten hinzu
        formatCols(j);
        table_chars+=4;
        printf("  ");
        table_chars+=2;
    }
    printf("\n");
    for(int j = 0; j < table_chars; j++){//Erzeugt einen Trennstrich
        printf("/");
    }
    printf("\n");
	consoleChangeColor(Default);
}

void printIterationLegend(){
	consoleChangeColor(Info);
	printf("[Legende]\n");
	printf("# gibt an, dass es keinen Vorg%cnger gibt\n",ae);
	printf("- gibt an, dass die Distanz auf unendlich gesetzt ist\n");
	printf("[ ] zeigen %cnderungen im jeweiligen Operationsschritt\n",AE);
	consoleChangeColor(Default);
}


void printIterationRow(int iteration, int maxVertices, int startVertex, int costs[], int costs_old[], int predecessors[], int predecessors_old[]){ //Fügt eine Zeile in der Tabelle hinzu
	consoleChangeColor(Info);
	printf("%i",iteration);
	consoleChangeColor(Default);
    for(int i = 0; i < 10 - getIntLen(iteration); i++){
        printf(" ");
    }
     printf("| ");
	formatUpdate(startVertex, costs, costs_old);
    for(int i = 0; i < maxVertices; i++){
		if(i == startVertex){
			continue;
		}
        formatUpdate(i, costs, costs_old);
    }
    printf(" | ");
    for(int i = 0; i < maxVertices; i++){
        formatUpdate(i, predecessors, predecessors_old);
    }
    printf("\n");
}