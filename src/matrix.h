//Ein Header für Funktionen der 2D-Arrays

//Da das übergeben von 2D Arrays (oder pointern auf pointer) doch eher mehr Probleme mit \
sich bringt verwenden wir einen linearisierten Array. D.h. die Zeilen wurden hintereinander gelegt
int getLinearizedArrayIndex(int zeilenIndex,int spaltenIntex,int spaltenAnzahl){
	return zeilenIndex*spaltenAnzahl+spaltenIntex;
}


void overwriteArray(int arrayToOverwrite[],int arrayToOverwriteWith[], int len){
	for(int i = 0;i<len;i++){
		arrayToOverwrite[i] = arrayToOverwriteWith[i];
	}
}